int sound_sensor= 11;
int relay1= 2;
int relay2= 3;//
int clap= 0;
long detection_range_start = 0;
long detection_range = 0;
boolean status_lights = false;
 
void setup() {
  pinMode(sound_sensor, INPUT);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);//
}
 
void loop() {
 
  int status_sensor = digitalRead(sound_sensor);
 
  if (status_sensor == 0)
  {
    if (clap == 0)
    {
      detection_range_start = detection_range = millis();
      clap++;
    }
    else if (clap > 0 && millis()-detection_range >= 50)
    {
      detection_range = millis();
      clap++;
    }
  }
 
  if (millis()-detection_range_start >= 400)
  {
    if (clap == 2)
    {
      if (!status_lights)
        {
          status_lights = true;
          digitalWrite(relay2, HIGH);
        }
        else if (status_lights)
        {
          status_lights = false;
          digitalWrite(relay2, LOW);
        }
    }
    else if (clap == 1)//
    {                    //
      if (!status_lights)//
      {                    //
        status_lights = true;//
        digitalWrite(relay1,HIGH);//
      }                           //
      else if (status_lights)//
      {                          //
        status_lights = false;//
        digitalWrite(relay1, LOW);//
      }                             //
    }                                 //
    clap = 0;
  }
}

// end code﻿
